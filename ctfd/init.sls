
ctfd.pkgs:
  pkg.installed:
    - pkgs:
      - gcc
      - libpq-dev
      - libffi-dev
      - libssl-dev
      - postgresql-common
      - python-dev
      - python-pip
      - redis

ctfd.git:
  git.latest:
    - name: https://github.com/ctfd/ctfd
    - target: /usr/src/ctfd
    - rev: 2.2.0

ctfd.reqs:
  pip.installed:
    - requirements: /usr/src/ctfd/requirements.txt
    - require:
      - git: ctfd.git
      - pkg: ctfd.pkgs
    - watch:
      - git: ctfd.git
      - pkg: ctfd.pkgs

ctfd.psycopg2:
  pip.installed:
    - name: psycopg2
    - require:
      - pip: ctfd.reqs
    - watch:
      - pip: ctfd.reqs

ctfd.user:
  user.present:
    - name: ctfd
    - home: /nonexistent
    - shell: /bin/false
    - system: True

/etc/default/ctfd:
  file.managed:
    - user: root
    - group: root
    - mode: 0600
    - contents_pillar: ctfd

/etc/systemd/system/ctfd.service:
  file.managed:
    - user: root
    - group: root
    - contents: |
        Description=CTFd
        After=network.target

        [Service]
        Type=simple
        User=ctfd
        Group=nogroup
        WorkingDirectory=/usr/src/ctfd
        EnvironmentFile=/etc/default/ctfd
        ExecStart=/usr/local/bin/gunicorn -uctfd -gnogroup --bind 127.0.0.1:8000 -w4 --worker-class gevent 'CTFd:create_app()'
        ExecReload=/bin/kill -s HUP $MAINPID
        ExecStop=/bin/kill -s TERM $MAINPID
        PrivateTmp=true

        [Install]
        WantedBy=multi-user.target
    - require:
      - user: ctfd.user
      - pip: ctfd.psycopg2
  module.run:
    - name: service.systemctl_reload
    - watch:
      - file: /etc/systemd/system/ctfd.service
  service.running:
    - name: ctfd
    - enable: True
    - unmask: True
    - watch_any:
      - pip: ctfd.psycopg2
      - file: /etc/default/ctfd
      - file: /etc/systemd/system/ctfd.service

/var/cache/ctfd:
  file.directory:
    - user: ctfd
    - group: nogroup
    - require:
      - user: ctfd.user

/var/log/ctfd:
  file.directory:
    - user: ctfd
    - group: nogroup
    - require:
      - user: ctfd.user

include:
  - nginx
