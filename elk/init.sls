#!jinja|yaml
#
# TODO
#
#  - let pillar provide plugins
#

elk-elasticsearch:
  pkg.installed:
    - name: elasticsearch
  service.running:
    - name: elasticsearch
    - enable: True
    - require:
      - pkg: elasticsearch
    - watch:
      - pkg: elasticsearch

elk-logstash-plugins:
  file.managed:
    - name: /root/logstash-input-relp-7.5.1.zip
    - user: 0
    - group: 0
    - source: salt://elk/files/logstash-input-relp-7.5.1.zip
  cmd.run:
    - name: /usr/share/logstash/bin/logstash-plugin install file:///root/logstash-input-relp-7.5.1.zip
    - require:
      - pkg: logstash-pkg
      - file: /root/logstash-input-relp-7.5.1.zip

elk-kibana:
  pkg.installed:
    - name: kibana
  service.running:
    - name: kibana
    - enable: True
    - require:
      - pkg: kibana
    - watch:
      - pkg: kibana

include:
  - logstash
  - nginx
