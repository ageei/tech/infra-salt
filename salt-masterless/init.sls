#!jinja|yaml

{% set master = salt.pillar.get('salt:master') %}


#
# On some systems, LXD adds the `.lxd` TLD to containers attached
# to the default network. This gets picked by salt-minion and used
# for the minion id. Remove it to keep things simple.
#
/etc/salt/minion_id:
  file.replace:
    - pattern: '\.lxd$'
    - repl: ''


python-pygit2:
  pkg.installed


/etc/salt/minion:
  file.append:
    #
    # WARNING This state is only used to create system images and we are
    # running masterless. We intentionally skip ext_pillar and our main
    # salt repository (the first one) since we use the local repos.
    #
    # Note that we could also do filtering in the salt:master pillar,
    # but doing the yaml->string conversion with multiple level and types
    # would be much more error-prone. We can keep it simpler here.
    #
    - text: |
       grains:
         env: {{ salt.environ.get('CI_ENVIRON', 'dev') }}
       {%- for k in master %}
       {%- if 'ext_pillar' != k %}
       {{ k }}:
         {%- for k1 in master[k] %}
         {%- if not (('gitfs_remotes' == k) and (0 == loop.index0)) %}
         - {{ k1 }}
         {%- endif %}
         {%- endfor %}
       {%- endif %}
       {%- endfor %}
