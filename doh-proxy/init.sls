
# we use python3, but salt.state.pip_state needs python2 installed
python3-pip:
  pkg.installed:
    - pkgs:
        - python-pip
        - python3-pip
        - python3-setuptools
        - python3-wheel

doh-proxy:
  pip.installed:
    - bin_env: /usr/bin/python3
    - require:
      - pkg: python3-pip


/etc/systemd/system/doh-stub.service:
  file.managed:
    - user: root
    - group: root
    - contents: |
        [Unit]
        Description=DoH stub
        After=syslog.target network.target

        [Service]
        Type=simple
        ExecStart=/usr/local/bin/doh-stub --listen-address 127.0.0.1 --listen-port 8053 --remote-address 9.9.9.9 --domain dns.quad9.net
        Restart=always
        User=nobody
        Group=nogroup

        [Install]
        WantedBy=multi-user.target
  module.run:
    - name: service.systemctl_reload
    - watch:
      - file: /etc/systemd/system/doh-stub.service
  service.running:
    - name: doh-stub
    - enable: True
    - watch:
      - pip: doh-proxy
      - file: /etc/systemd/system/doh-stub.service
