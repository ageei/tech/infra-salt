
rsyslog:
  pkg.installed:
    - pkgs:
      - rsyslog
      - rsyslog-relp
  file.managed:
    - name: /etc/rsyslog.d/90-local.conf
    - user: root
    - group: root
    - contents_pillar: rsyslog:config
    - require:
      - pkg: rsyslog
  service.running:
    - name: rsyslog
    - enable: True
    - require:
      - pkg: rsyslog
    - watch_any:
      - file: /etc/rsyslog.d/90-local.conf
      - pkg: rsyslog
