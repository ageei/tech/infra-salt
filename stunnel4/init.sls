#!jinja|yaml
#
# The stunnel state is a bit convoluted. The reason is
# that, if we are creating an image, we don't want to
# install the X.509 files and, by extension, don't want
# to start the service to prevent it from failing due
# to missing keys.
#


stunnel4:
  pkg.installed: []
{% if '' != salt.environ.get('CI_IMAGING', '') %}
  service.running:
    - enable: True
{% else %}
  service.enabled:
{% endif %}
{% if salt.pillar.get('stunnel') %}
    - require:
{%- for tunnel in salt.pillar.get('stunnel') %}
      - file: /etc/stunnel/{{ tunnel }}.conf
{% endfor %}
{% endif %}
{% if salt.pillar.get('stunnel') %}
    - watch:
{%- for tunnel in salt.pillar.get('stunnel') %}
      - file: /etc/stunnel/{{ tunnel }}.conf
{% endfor %}
{% endif %}


/etc/default/stunnel4:
  file.managed:
    - user: root
    - group: root
    - contents: |
        ENABLED=1
    - require:
      - pkg: stunnel4


{% for tunnel in salt.pillar.get('stunnel') %}
/etc/stunnel/{{ tunnel }}.conf:
  file.managed:
    - user: root
    - group: root
    - contents_pillar: stunnel:{{ tunnel }}:config
    - require:
      - pkg: stunnel4
    - watch_in:
      - service: stunnel4


{% if 'prd' != salt.pillar.get('env', 'dev') %}
{{ salt.pillar.get('stunnel:' + tunnel)['cert']['file'] }}:
  file.managed:
    - user: root
    - group: root
    - mode: '0644'
    - contents_pillar: stunnel:{{ tunnel }}:cert:x509
    - require:
      - pkg: stunnel4
    - watch_in:
      - service: stunnel4


{{ salt.pillar.get('stunnel:' + tunnel)['key']['file'] }}:
  file.managed:
    - user: root
    - group: root
    - mode: '0400'
    - contents_pillar: stunnel:{{ tunnel }}:key:x509
    - require:
      - pkg: stunnel4
    - watch_in:
      - service: stunnel4
{% endif %}
{% endfor %}
