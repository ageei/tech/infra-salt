#!jinja|yaml

/etc/rc.local:
  file.managed:
    - user: root
    - group: root
    - mode: '0755'
    - contents: |
        #!/bin/sh -e
        if [ ! -f /etc/ssh/ssh_host_rsa_key ]; then
            ssh-keygen -A
            /etc/init.d/ssh restart
        fi
{%- for _, blk in salt.pillar.get('rc.local', {}).items() %}
        {{ blk | indent(8) }}
{%- endfor %}
