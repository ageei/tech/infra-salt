
iptables-persistent:
  pkg.installed

#
# Write the iptables rules and load them when they change.
#
/etc/iptables/rules.v4:
  file.managed:
    - user: root
    - group: root
    - contents_pillar: rules.v4
  cmd.run:
    - name: iptables-restore /etc/iptables/rules.v4
    - require:
      - pkg: iptables-persistent
    - watch:
      - file: /etc/iptables/rules.v4
