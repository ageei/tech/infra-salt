#!jinja|yaml

#
# Removing the default user (`ubuntu`) *during* a cloud-init
# deployment makes it fail and, subsequently, causes the image
# creation to fail. The default user will be removed with the
# first regular deployment.
#

# We don't want no unused user account laying around.
{% if '' == salt.environ.get('CI_IMAGING', '') %}
ubuntu user:
  user.absent:
    - name: ubuntu
    - purge: True
    - force: True
{% endif %}
