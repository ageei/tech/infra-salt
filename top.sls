---

base:
  # Each system builds on the following recipes...
  '*':
    - apt
    - cert
    - cleanup
    - dhparam
    - rc-local
    - rsyslog
    - rules
    - salt.minion
    - sudoers
    - telegraf
    - users

  # ... and then add their own application.

  'inf-geo':
    - dnslog
    - doh-proxy
    - grub
    - ifupdown
    - lxd
    - lxd-subids
    - openssh
    - openssh.config
    - unbound

  'inf-gw':
    - dnsmasq
    - dnslog
    - doh-proxy
    - ifupdown
    - unbound
    # openssh.config must come before haproxy so the port is changed
    # TODO check if the `openssh` is necessary
    - openssh
    - openssh.config
    - haproxy

  'inf-apt':   [ 'apt-cacher-ng' ]
  'inf-elk':   [ 'elk' ]
  'inf-salt':  [ 'salt.master' ]
  'inf-tig':   [ 'tig' ]
  'pub-wiki':  [ 'le-snakeoil', 'gitit' ]
  'dev-lxd':   [ 'lxd', 'gitlab-runner' ]
  'ctf-ctfd':  [ 'postgres', 'le-snakeoil', 'ctfd' ]
  'ctf-web':   [ 'docker', 'haproxy.install', 'ctf-web', 'haproxy.service' ]
  # TODO ci
  'ctf-pwn':   [ 'lxd', 'ctfsh' ]
