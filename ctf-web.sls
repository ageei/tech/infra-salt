#
# - Installs the `chals` service to manage docker-compose -managed
#   containers.
# - Installs a configuration file for haproxy. We don't want it managed
#   by saltstack since it's managed by the CTF's CI system.
#

/etc/docker-compose.yml:
  file.managed:
    - user: root
    - group: root
    - mode: '0644'
    - contents_pillar: chals:docker-compose
    - unless: "test -f /etc/docker-compose.yml && grep -q 'automatically generated' /etc/docker-compose.yml"


/etc/systemd/system/chals.service:
  file.managed:
    - user: root
    - group: root
    - mode: '0644'
    - require:
      - file: /etc/docker-compose.yml
    - contents: |
        [Unit]
        Description=ctf-web
        Requires=docker.service
        After=docker.service

        [Service]
        Restart=always

        # Remove old containers, images and volumes
        ExecStartPre=/usr/local/bin/docker-compose -f /etc/docker-compose.yml down -v --remove-orphans
        ExecStartPre=/usr/local/bin/docker-compose -f /etc/docker-compose.yml rm -v
        ExecStartPre=-/bin/sh -c 'docker volume rm $(docker volume ls -q)'
        ExecStartPre=-/bin/sh -c 'docker rmi $(docker images | grep "<none>" | awk \'{print $3}\')'
        ExecStartPre=-/bin/sh -c 'docker rm -v $(docker ps -aq)'

        # Compose up
        ExecStart=/usr/local/bin/docker-compose -f /etc/docker-compose.yml up

        # Compose down, remove containers and volumes
        ExecStop=/usr/local/bin/docker-compose -f /etc/docker-compose.yml down -v --remove-orphans

        [Install]
        WantedBy=multi-user.target
  module.run:
    - name: service.systemctl_reload
    - watch:
      - file: /etc/systemd/system/chals.service
  service.running:
    - name: chals
    - enable: True
    - require:
      - file: /etc/systemd/system/chals.service
      - file: /etc/docker-compose.yml
    - watch_any:
      - file: /etc/systemd/system/chals.service
      - file: /etc/docker-compose.yml


/etc/haproxy/haproxy.cfg:
  file.managed:
    - user: root
    - group: root
    - mode: '0644'
    - contents_pillar: chals:haproxy
    # Do not override the CI system's configuration.
    - unless: grep -q backend /etc/haproxy/haproxy.cfg
  cmd.run:
    - name: systemctl restart haproxy
    - require:
      - file: /etc/haproxy/haproxy.cfg
    - watch:
      - file: /etc/haproxy/haproxy.cfg


# TODO
# fetch repo
# fixme: how does it interact with image building?


#
# Let the project's CI system do its thing.
#
# TODO create dedicated user and sudo profile
#
update configs:
  cron.present:
    - name: test -f /root/defis/tools/update.sh && sh /root/defis/tools/update.sh
    - user: root
    - minute: '*/15'
