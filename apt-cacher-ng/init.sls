# Installs and configures apt-cacher-ng
# TODO apt-cacher-ng only, -nginx

apt-cacher-ng:
  pkg.installed: []
  file.managed:
    - name: /etc/apt-cacher-ng/acng.conf
    - source: salt://apt-cacher-ng/acng.conf
    - user: root
    - group: root
    - mode: 644
    - require:
      - pkg: apt-cacher-ng
  service.running:
    - name: apt-cacher-ng
    - enable: True
    - reload: False
    - require:
      - pkg: apt-cacher-ng
    - watch:
      - file: /etc/apt-cacher-ng/acng.conf
      - pkg: apt-cacher-ng


nginx-light:
  pkg.installed: []
  file.managed:
    - name: /etc/nginx/sites-available/default
    - source: salt://apt-cacher-ng/nginx.conf
    - user: root
    - group: root
    - mode: 644
    - require:
      - pkg: nginx-light
  service.running:
    - name: nginx
    - enable: True
    - reload: True
    - require:
      - pkg: nginx-light
    - watch_any:
      - file: /etc/nginx/sites-available/default
      - pkg: nginx-light
