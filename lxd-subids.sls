#!jinja|yaml

{% for id in [ 'gid', 'uid' ] %}
/etc/sub{{ id }}:
  file.replace:
    - pattern: '^lxd:.*'
    - repl: 'lxd:{{ salt.pillar.get('lxd.subids:base') }}:{{ salt.pillar.get('lxd.subids:size') }}'
{% endfor %}

subids restart:
  service.running:
    - name: lxd
    - watch_any:
      - file: /etc/subgid
      - file: /etc/subuid
