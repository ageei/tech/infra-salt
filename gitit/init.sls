#!jinja|yaml

packages:
  pkg.installed:
    - pkgs:
        - gitit
        - pandoc
        - s5
        - texlive-fonts-recommended
        - texlive-latex-base
        - texlive-latex-recommended
        - texlive-pictures


gitit user:
  user.present:
    - name: gitit
    - shell: /bin/sh
    - home: /nonexistent
    - system: True

/var/gitit:
  file.directory


/var/cache/gitit:
  file.directory:
    - user: gitit
    - group: nogroup
    - require:
      - user: gitit

/var/log/gitit.log:
  file.managed:
    - user: gitit
    - group: nogroup
    - require:
      - user: gitit


wiki theme:
  git.latest:
    - name: https://gitlab.com/ageei/wiki.ageei.org-theme
    - target: /srv/gitit

wiki pages:
  git.latest:
    - name: https://gitlab.com/ageei/wiki.ageei.org
    - target: /srv/gitit/wikidata
    - require:
      - git: 'wiki theme'

gitit-updater user:
  user.present:
    - name: gitit-updater
    - home: /home/gitit-updater
    - system: True

/srv/gitit:
  file.directory:
    - user: gitit-updater
    - group: nogroup
    - recurse:
      - user
      - group
    - require:
      - user: gitit-updater


/usr/local/etc/gitit.conf:
  file.managed:
    - user: root
    - group: root
    - contents_pillar: gitit.config

/home/gitit-updater/refresh.sh:
  file.managed:
    - user: root
    - group: root
    - source: salt://gitit/files/refresh.sh
    - require:
      - user: gitit-updater

gitit cron:
  cron.present:
    - name: /bin/sh /home/gitit-updater/refresh.sh
    - user: gitit-updater
    - minute: '*/15'
    - require:
      - user: gitit-updater


/etc/systemd/system/gitit.service:
  file.managed:
    - user: root
    - group: root
    - contents: |
        [Unit]
        Description=Gitit wiki
        After=network.target

        [Service]
        ExecStart=/usr/bin/gitit -f /usr/local/etc/gitit.conf
        WorkingDirectory=/srv/gitit
        User=gitit
        Group=nogroup
        Restart=always

        [Install]
        WantedBy=multi-user.target
  module.run:
    - name: service.systemctl_reload
    - watch:
      - file: /etc/systemd/system/gitit.service
  service.running:
    - name: gitit
    - enable: True
    - require:
      - module: /etc/systemd/system/gitit.service
      - pkg: packages
      - file: /etc/systemd/system/gitit.service
      - file: /usr/local/etc/gitit.conf
    - watch_any:
      - pkg: packages
      - file: /etc/systemd/system/gitit.service
      - file: /usr/local/etc/gitit.conf


include:
  - nginx
