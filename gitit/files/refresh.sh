#!/bin/sh
##
set -eu

cd /srv/gitit
if [ ! -d ./.git/ ]; then
	rm -rf /srv/gitit/*
	git init
	git remote add origin https://gitlab.com/ageei/wiki.ageei.org-theme
	git fetch
	git reset origin/master
	git checkout .
	git clone https://gitlab.com/ageei/wiki.ageei.org wikidata
fi

git pull
cd wikidata
git pull
