#!jinja|yaml

#
# this block actually does not change apt's configuration
# it just fetches the GPG keys for the next block :)
#
{%- for vendor in salt.pillar.get('apt:vendors') %}
pkgrepo {{ vendor }}:
  pkgrepo.managed:
    - file: /etc/apt/.sources.list
    - keyid: '{{ salt.pillar.get('apt:vendors:' + vendor)['keyid'] }}'
    - keyserver: keyserver.ubuntu.com
{%- endfor %}


/etc/apt/sources.list:
  file.managed:
    - user: root
    - group: root
    - contents: |
{%- for archive in salt.pillar.get('apt:archives') %}
        {{ archive }}
{%- endfor -%}
{%- for vendor in salt.pillar.get('apt:common') %}
        {{ salt.pillar.get('apt:vendors:' + vendor + ':source') }}
{%- endfor %}
{%- for vendor in salt.pillar.get('apt:local') %}
        {{ salt.pillar.get('apt:vendors:' + vendor + ':source') }}
{%- endfor %}
  cmd.run:
    - name: apt-get update
    - watch:
      - file: /etc/apt/sources.list
