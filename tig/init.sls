#!jinja|yaml
#
# TODO
#
# - move config to a pillar by creating a formula
#

influxdb:
  pkg.installed: []
  service.running:
    - enable: True
    - require:
      - pkg: influxdb
    - watch:
      - pkg: influxdb


grafana:
  pkg.installed: []
  service.running:
    - name: grafana-server
    - enable: True
    - require:
      - pkg: grafana
    - watch:
      - pkg: grafana

include:
  - nginx
