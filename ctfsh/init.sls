

ctfsh-pkgs:
  pkg.installed:
    - pkgs:
      - figlet
      - python3-pylxd


#
# Install so that the only thing the `ctfsh`
# user can write to is `~/.config`.
#
/home/ctf:
  file.directory:
    - user: root
    - group: root

ctfsh:
  git.latest:
    # TODO change src
    - name: https://gitlab.com/pgregoire-ci/ctfsh
    - target: /usr/src/ctfsh
    - require:
      - pkg: ctfsh-pkgs
  user.present:
    - name: ctf
    - shell: /usr/src/ctfsh/shell.py
    - home: /home/ctf
    - createhome: False
    - groups:
      - lxd
    - require:
      - git: ctfsh
      - file: /home/ctf
  file.directory:
    - name: /home/ctf/.config
    - user: ctf
    - group: root
    - require:
      - user: ctfsh
      - file: /home/ctf


/usr/src/ctfsh/shell.py:
  file.managed:
    - user: root
    - group: root
    - mode: '0755'
