#!jinja|yaml
#
# TODO
#
# - config.toml from pillar
#

gitlab-runner:
  pkg.installed: []
  service.running:
    - enable: True
    - require:
      - pkg: gitlab-runner
    - watch:
      - pkg: gitlab-runner

/opt/lxd-runner-git:
  git.latest:
    - target: /opt/lxd-runner-git
    - name: https://gitlab.com/pgregoire/lxd-runner
  file.symlink:
    - name: /opt/lxd-runner
    - target: /opt/lxd-runner-git
    - require:
      - git: /opt/lxd-runner-git 
