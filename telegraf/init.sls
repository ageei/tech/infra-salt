# TODO common in pillar

telegraf:
  pkg.installed


/etc/telegraf/telegraf.d/common.conf:
  file.managed:
    - user: root
    - group: root
    - contents: |
        [[inputs.net]]
        [[inputs.iptables]]
          use_sudo = true
          table = "filter"
          chains = ["INPUT", "FORWARD", "OUTPUT"]
        [[outputs.influxdb]]
          urls = ["http://tig.inf:8086"]


/etc/telegraf/telegraf.d/local.conf:
  file.managed:
    - user: root
    - group: root
    - contents_pillar: telegraf:config


telegraf-service:
  service.running:
    - name: telegraf
    - enable: True
    - watch_any:
      - file: /etc/telegraf/telegraf.d/common.conf
      - file: /etc/telegraf/telegraf.d/local.conf
      - pkg: telegraf
