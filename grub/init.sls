
grub-pc:
  pkg.installed


/etc/default/grub:
  file.append:
    - text: '[ ! -f /etc/default/grub.local ] || . /etc/default/grub.local'


/etc/default/grub.local:
  file.managed:
    - user: root
    - group: root
    - contents_pillar: grub:config
  cmd.run:
    - name: update-grub
    - watch:
      - file: /etc/default/grub.local
