#!jinja|yaml
#
# Installs and configure docker.
#

/etc/docker:
  file.directory

/etc/docker/daemon.json:
  file.managed:
    - contents: '{ "iptables": false }'


docker:
  pkg.installed:
    - pkgs:
      - containerd.io
      - docker-ce
      - docker-ce-cli
  service.running:
    - enable: True
    - require:
      - pkg: docker
    - watch:
      - pkg: docker


docker-compose:
  pkg.installed:
    - name: python-pip
  pip.installed:
    - require:
      - pkg: docker-compose
