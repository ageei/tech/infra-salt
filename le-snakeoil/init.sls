#!jinja|yaml
#
# Install snakeoil X.509 certificates.
#

certbot:
  pkg.installed


{% for domain, _ in salt.pillar.get('snakeoil', {}).items() %}
/etc/letsencrypt/live/{{ domain }}:
  file.directory:
    - user: root
    - group: root
    - mode: '0755'
    - makedirs: True
  cmd.run:
    - name: openssl req -x509 -newkey rsa:2048 -keyout /etc/letsencrypt/live/{{ domain }}/privkey.pem -out /etc/letsencrypt/live/{{ domain }}/fullchain.pem -days 3650 -nodes -subj "/CN={{ domain }} snakeoil"
    # Don't override existing keys.
    - unless: test -f /etc/letsencrypt/live/{{ domain }}/privkey.pem
{% endfor %}
