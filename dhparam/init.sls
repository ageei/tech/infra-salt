#!jinja|yaml

#
# Install new Diffie-Hellman parameters at a given path.
#

{% for _, path in salt.pillar.get('dhparam', {}).items() %}
{{ path }}:
  cmd.run:
    - name: openssl dhparam -out {{ path }} 2048
    - unless: test -f {{ path }}
{% endfor %}
