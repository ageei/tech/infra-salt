
wireshark-common:
  pkg.installed


/etc/systemd/system/dnslog.service:
  file.managed:
    - user: 0
    - group: 0
    - contents: |
        [Unit]
        Description=DNS logging
        After=syslog.target network.target

        [Service]
        Type=simple
        # TODO adjust filesize
        ExecStart=/usr/bin/dumpcap -i nflog:53 -w /var/log/dnslog.pcap -b filesize:32768
        Restart=always

        [Install]
        WantedBy=multi-user.target

  module.run:
    - name: service.systemctl_reload
    - watch:
      - file: /etc/systemd/system/dnslog.service

  service.running:
    - name: dnslog.service
    - enable: True
    - watch:
      - pkg: wireshark-common
      - file: /etc/systemd/system/dnslog.service


#/etc/logrotate.d/dnslog:
#  file.managed:
#    - user: 0
#    - group: 0
#    - contents_pillar: dnslog:logrotate
